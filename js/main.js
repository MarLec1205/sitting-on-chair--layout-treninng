document.addEventListener("DOMContentLoaded", function(){

    function Slider(selector, pause) {
        this.selector = selector;
        this.slider = document.querySelector(selector);
        this.currentSlide = 0;
        this.slides = this.slider.querySelectorAll('.slider');
        this.btnPrev = this.slider.querySelector('.left ');
        this.btnNext = this.slider.querySelector('.right');
        this.bindButtons();
        this.pause = pause;

        if (this.pause !== 0) {
            this.time = setTimeout(() => {
                this.nextSlide();
            }, this.pause)
        }
    }

    Slider.prototype.changeSlide = function(nr) {
        this.currentSlide = nr;
        for (const slide of this.slides) {
            slide.classList.remove("active");
        }
        this.slides[this.currentSlide].classList.add("active");

        if (this.pause !== 0) {
            clearTimeout(this.time);
            this.time = setTimeout(() => {
              this.nextSlide();
            }, this.pause)
        }
    }

    Slider.prototype.prevSlide = function() {
        this.currentSlide--;
        if (this.currentSlide < 0) {
            this.currentSlide = this.slides.length - 1;
        }
        this.changeSlide(this.currentSlide);
    }

    Slider.prototype.nextSlide = function() {
        this.currentSlide++;
        if (this.currentSlide > this.slides.length - 1) {
            this.currentSlide = 0;
        }
        this.changeSlide(this.currentSlide);
    };
    Slider.prototype.bindButtons = function() {
        this.btnPrev.addEventListener("click", () => {
            this.prevSlide();
        });
        this.btnNext.addEventListener("click", () => {
            this.nextSlide();
        })
    };
    const slide = new Slider("#sit-on-our-chair",2000);

    const dropdown = document.querySelector(".dropdown-item");
    const drop=document.querySelector(".dropdown");

    dropdown.addEventListener("mouseover", function(){
        drop.classList.add("dropdown-show");
    });
    dropdown.addEventListener("mouseout", function () {
        drop.classList.remove("dropdown-show");


    });
    const selectFirst = document.querySelector("#type");
    console.log(selectFirst.children);
    const selectColor = document.querySelector("#color");
    const selectMaterial = document.querySelector("#material");
    console.log(selectColor.innerHTML);
    const box = [];
    box.push(document.querySelector("#box1"));
    box.push(document.querySelector("#box2"));

    for (let el of box) {
        el.addEventListener('mouseenter', function () {
            const textHover = el.querySelector('.hover-text');
            textHover.style.opacity = '0';
            textHover.style.visibility = 'hidden';
            textHover.style.bottom = '-20px';
        });
        el.addEventListener('mouseleave', function () {
            const textHover = el.querySelector('.hover-text');
            textHover.style.opacity = '1';
            textHover.style.visibility = 'visible';
            textHover.style.bottom = '20px';
        })
    }



});





